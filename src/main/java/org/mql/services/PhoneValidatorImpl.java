package org.mql.services;

import jakarta.jws.WebService;

@WebService(endpointInterface = "org.mql.services.PhoneValidator")
public class PhoneValidatorImpl implements PhoneValidator {
	
	@Override
    public boolean validatePhone(String phoneNumber) {
        if (phoneNumber.length() != 10) {
            return false;
        }

        if (!phoneNumber.startsWith("0")) {
            return false;
        }

        for (int i = 0; i < phoneNumber.length(); i++) {
            if (!Character.isDigit(phoneNumber.charAt(i))) {
                return false;
            }
        }

        return true;
    }
}

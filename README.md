# Phone Validator Web Service

This project implements a SOAP-based web service for validating phone numbers.

## Features

- Exposes a single operation: `validatePhone`
- Takes a single parameter: `phoneNumber` (string)
- Returns a boolean value: `true` if valid, `false` otherwise
- Validates phone numbers based on a simple set of rules:
    - Must be 10 digits long
    - Must start with 0 (configurable)
    - Must contain only digits



## Technologies

- Java 11 (configurable)
- JAX-WS

## Build and Deploy

1. Install Maven (optional).
2. Clone this repository.
3. Run `mvn clean install` in the project directory (optional).
4. Deploy the generated WAR file to a servlet container like Tomcat.
5. Access the service endpoint at: `http://localhost:8080/phone-validator/validatePhone`

## Usage

1. Create a SOAP client using a tool like SoapUI.
2. Set the endpoint URL to `http://localhost:8080/phone-validator/validatePhone` (configurable).
3. Send a SOAP request which is a POST request with the following request Body :

```xml
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ws="http://services.mql.org/">
   <soapenv:Header/>
   <soapenv:Body>
      <ws:validatePhone>
         <arg0>+1234567890</arg0>
      </ws:validatePhone>
   </soapenv:Body>
</soapenv:Envelope>
```

4. Replace `+1234567890` with the phone number to validate.
5. Send the request and analyze the response.

The response will be an XML message containing a return element with a boolean value (true if valid, false otherwise):

```xml
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ws="http://services.mql.org/">
   <soapenv:Header/>
   <soapenv:Body>
      <ws:validatePhoneResponse>
         <return>true</return>
      </ws:validatePhoneResponse>
   </soapenv:Body>
</soapenv:Envelope>